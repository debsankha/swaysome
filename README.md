# Swaysome

## Description

This binary helps you configure sway to work a bit more like [AwesomeWM](https://awesomewm.org/). This currently means
workspaces that are name-spaced on a per-screen basis.  
example: with workspace `11` on the first screen, and workspace `21` on the second screen, typing the `swaysome`
shortcut to get workspace `1` would lead you to workspace `11` if your focus is on the first screen, and workspace `21`
is the focus is on the second one.

`swaysome` also supports what we call "workspace groups". These are groups of workspaces sharing the same tens number
and tied to an output.  
By default, `swaysome init` will create a workspace group per active output, but you may create other groups while working.

Here is a common use-case for this:

* `output-1`:
  * workspace group 1:
    * workspace `11`: chats
    * workspace `12`: emails
* `output-2`:
  * workspace group 2:
    * workspace `21`: IDE for first project
    * workspace `22`: browser for first project
    * workspace `23`: terminals for first project
  * workspace group 3:
    * workspace `31`: IDE for second project
    * workspace `32`: browser for second project
    * workspace `33`: terminals for second project

That way, when `output-2` is focused on workspace group 2, be it workspace `21` or `22`, the quick `$mod+<number>`
shortcut won't leave workspace group 2, allowing you to open multiple projects in parallel without the hassle of
manually remembering how to namespace them.


`swaysome` may also work with i3, but this is untested.

`swaysome` should be compatible with [sworkstyle](https://lib.rs/crates/sworkstyle).


## Installation


Arch Linux: Found on the AUR as [swaysome-git](https://aur.archlinux.org/packages/swaysome-git).

If you have Rust installed, then you can just `cargo install swaysome` and you're good to go.

Otherwise, you may grab a [pre-built
binary](https://gitlab.com/hyask/swaysome/-/jobs/artifacts/master/raw/target/release/swaysome?job=build:cargo) from the
CI and put it in your `$PATH`.

**WARNING**: please double-check that your `swaysome` binary is in `sway`'s `$PATH`. Depending on your setup, the
`$PATH` you have in your shell may not be the same as `sway`'s, and if `swaysome` can't be called by `sway`, the
symptoms will only look like non-functional shortcuts.  
If you're in this situation, a quick workaround is to call `swaysome` with its full absolute path from `sway`'s config
to check that everything works before fixing your `$PATH` issue.


## Usage

Then create the file (and the directory if needed) `~/.config/sway/config.d/swaysome.conf` and paste this inside:
> Use (un)bindcode or (un)bindsym, depending on what you used in your main sway config file.
```
# Change focus between workspaces
unbindcode $mod+1
unbindcode $mod+2
unbindcode $mod+3
unbindcode $mod+4
unbindcode $mod+5
unbindcode $mod+6
unbindcode $mod+7
unbindcode $mod+8
unbindcode $mod+9
unbindcode $mod+0
bindcode $mod+1 exec "swaysome focus 1"
bindcode $mod+2 exec "swaysome focus 2"
bindcode $mod+3 exec "swaysome focus 3"
bindcode $mod+4 exec "swaysome focus 4"
bindcode $mod+5 exec "swaysome focus 5"
bindcode $mod+6 exec "swaysome focus 6"
bindcode $mod+7 exec "swaysome focus 7"
bindcode $mod+8 exec "swaysome focus 8"
bindcode $mod+9 exec "swaysome focus 9"
bindcode $mod+0 exec "swaysome focus 0"

# Move containers between workspaces
unbindcode $mod+Shift+1
unbindcode $mod+Shift+2
unbindcode $mod+Shift+3
unbindcode $mod+Shift+4
unbindcode $mod+Shift+5
unbindcode $mod+Shift+6
unbindcode $mod+Shift+7
unbindcode $mod+Shift+8
unbindcode $mod+Shift+9
unbindcode $mod+Shift+0
bindcode $mod+Shift+1 exec "swaysome move 1"
bindcode $mod+Shift+2 exec "swaysome move 2"
bindcode $mod+Shift+3 exec "swaysome move 3"
bindcode $mod+Shift+4 exec "swaysome move 4"
bindcode $mod+Shift+5 exec "swaysome move 5"
bindcode $mod+Shift+6 exec "swaysome move 6"
bindcode $mod+Shift+7 exec "swaysome move 7"
bindcode $mod+Shift+8 exec "swaysome move 8"
bindcode $mod+Shift+9 exec "swaysome move 9"
bindcode $mod+Shift+0 exec "swaysome move 0"

# Focus workspace groups
unbindcode $mod+Ctrl+1
unbindcode $mod+Ctrl+2
unbindcode $mod+Ctrl+3
unbindcode $mod+Ctrl+4
unbindcode $mod+Ctrl+5
unbindcode $mod+Ctrl+6
unbindcode $mod+Ctrl+7
unbindcode $mod+Ctrl+8
unbindcode $mod+Ctrl+9
unbindcode $mod+Ctrl+0
bindcode $mod+Ctrl+1 exec "swaysome focus-group 1"
bindcode $mod+Ctrl+2 exec "swaysome focus-group 2"
bindcode $mod+Ctrl+3 exec "swaysome focus-group 3"
bindcode $mod+Ctrl+4 exec "swaysome focus-group 4"
bindcode $mod+Ctrl+5 exec "swaysome focus-group 5"
bindcode $mod+Ctrl+6 exec "swaysome focus-group 6"
bindcode $mod+Ctrl+7 exec "swaysome focus-group 7"
bindcode $mod+Ctrl+8 exec "swaysome focus-group 8"
bindcode $mod+Ctrl+9 exec "swaysome focus-group 9"
bindcode $mod+Ctrl+0 exec "swaysome focus-group 0"

# Move containers to other workspace groups
unbindcode $mod+Ctrl+Shift+1
unbindcode $mod+Ctrl+Shift+2
unbindcode $mod+Ctrl+Shift+3
unbindcode $mod+Ctrl+Shift+4
unbindcode $mod+Ctrl+Shift+5
unbindcode $mod+Ctrl+Shift+6
unbindcode $mod+Ctrl+Shift+7
unbindcode $mod+Ctrl+Shift+8
unbindcode $mod+Ctrl+Shift+9
unbindcode $mod+Ctrl+Shift+0
bindcode $mod+Ctrl+Shift+1 exec "swaysome move-to-group 1"
bindcode $mod+Ctrl+Shift+2 exec "swaysome move-to-group 2"
bindcode $mod+Ctrl+Shift+3 exec "swaysome move-to-group 3"
bindcode $mod+Ctrl+Shift+4 exec "swaysome move-to-group 4"
bindcode $mod+Ctrl+Shift+5 exec "swaysome move-to-group 5"
bindcode $mod+Ctrl+Shift+6 exec "swaysome move-to-group 6"
bindcode $mod+Ctrl+Shift+7 exec "swaysome move-to-group 7"
bindcode $mod+Ctrl+Shift+8 exec "swaysome move-to-group 8"
bindcode $mod+Ctrl+Shift+9 exec "swaysome move-to-group 9"
bindcode $mod+Ctrl+Shift+0 exec "swaysome move-to-group 0"

# Move focused container to next output
bindcode $mod+o exec "swaysome next-output"

# Move focused container to previous output
bindcode $mod+Shift+o exec "swaysome prev-output"

# Init workspaces for every screen
exec "swaysome init 1"
```

Finally append your `sway` configuration with this:
```
include ~/.config/sway/config.d/*.conf
```

You should end-up with workspaces from `1` to `0`, prefixed with a screen index,
giving you workspace `11` on the first screen, and workspace `21` on the second
one, both accessible with shortcut `$mod+1` when focused on the right output.

The `init` command simply walks through every screen to initialize a prefixed
workspace. It does it backwards so that you end-up focused on the first screen,
as usual.


## Exhaustive swaysome commands list

* `move [name]`: move the focused container to `[name]` on the same group
* `move-to-group [id]`: move the focused container to the same relative workspace on the `[id]` workspace group
* `next-output`: move the focused container to the next output
* `prev-output`: move the focused container to the previous output
* `focus [name]`: change focus to `[name]` on the same group
* `focus-all-outputs [name]`: change all outputs focus to `[name]`
* `focus-group [id]`: change focus to group `[id]`
* `next-group`: change focus to the next workspace group
* `previous-group`: change focus to the previous workspace group
* `init [name]`: cycle all outputs to create a default workspace with name `[name]`
* `rearrange-workspaces` Rearrange already opened workspaces to the correct outputs, useful when plugging or unplugging screens


## Breaking changes

* Starting with 2.0, `next_output` and `prev_output` have been changed to `next-output` and `prev-output`.
